FROM hypriot/rpi-mysql
ENV MYSQL_ROOT_PASSWORD root
RUN export DEBIAN_FRONTEND=noninteractive && \
  apt-get update && apt-get install -y \
  vim \
  && \
  apt-get clean && rm -rf /var/lib/apt/lists/*
